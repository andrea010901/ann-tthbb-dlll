#!/usr/bin/env python
import os
import csv
import numpy
import pandas

# imports with keras
import keras
import keras.models as models
from keras import backend as K

## limit gpu usage
#import tensorflow as tf

class DNN():
    def __init__(self, dataframe, variables,
            outputsnodes, modelDirectory
            ):
        # list of input variables
        self.dataframe = dataframe
        self.variables = variables
        self.modelDirectory =  modelDirectory

        # load data set
        self.data = self._load_datasets()
        self.event_classes = outputsnodes

    def _load_datasets(self):
        ''' load data set '''
        return DataFrame(
            dataframe       = self.dataframe,
            variables       = self.variables,
            modelDirectory  = self.modelDirectory)

    def load_trained_model(self):
        ''' load an already trained model '''
        checkpoint_path = self.modelDirectory+'/model.h5'

        # get the model
        self.model = keras.models.load_model(str(checkpoint_path))

        self.model.summary()

        # save predicitons
        self.model_prediction_vector = self.model.predict(self.data.df[self.variables].values)

        return self.model_prediction_vector

    def predict_events(self ):
        events = self.data.df[self.variables]
        print(str(events.shape[0]) + ' events.')

        for index, row in events.iterrows():
            print('========== DNN output ==========')
            print('Event: '+str(index))
            print(row)
            print('-------------------->')
            print(row.values)
            print('-------------------->')
            output = self.model.predict(numpy.array([list(row.values)]))[0]
            for idx, node in enumerate(self.event_classes):
                print(str(node)+' node: '+str(output[idx]))
            print('-------------------->')

class DataFrame():
    def __init__(self, dataframe, variables, modelDirectory):

        norm = pandas.DataFrame(index=['mu', 'std'], columns=variables)
        unnormed_df = dataframe.copy()

        with open(modelDirectory+'/variable_norm.csv') as csv_file:
            csv_reader = csv.reader(csv_file, delimiter=',')
            for idx, row in enumerate(csv_reader):
                if idx != 0:
                   norm[row[0]]['mu'] = row[1]
                   norm[row[0]]['std'] = row[2]

        norm[variables] = norm[variables].astype(float)

        dataframe[variables] = (dataframe[variables] - norm[variables].iloc[0])/norm[variables].iloc[1]
        self.df = dataframe

def DNN_outputs_nparray(dataFrame, model, inputs,outputs):

    # init DNN class
    dnn = DNN(

      dataframe = dataFrame,
      variables = inputs,
      outputsnodes = outputs,
      modelDirectory = model,
    )

    # load the trained model
    predictions = numpy.array(dnn.load_trained_model(), dtype='float64')

#    dnn.predict_events()

    d = [(str(o),predictions.dtype) for o in outputs]
    predictions.dtype = d
    return predictions

